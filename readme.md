# Simple Admin for CodeIgniter

Time after time I have been making simple administration interfaces/backends to different project, copying the code from the previous project to the next and changed what was needed to make it work in the new project.

To save time, I finally made a very simple admin skeleton, that I now can simply merge it into any new project and start adding whatever features I need.

I believe there is other developers out there, with the same "issue" so I decided to share my code.

## This is included

Simple Admin for CodeIgniter is like the name says, very simple and small. You get a base to use for your admin backend.

* Login screen
* Secure admin area with included user managment

That is all. The rest is up to you to add.

## Requires

### Server 
* PHP 5.3+
* MySQL database

### CodeIgniter
* CI v2.1.4

## Installation

Download the files and move to the correct folders. The download already have the correct folder setup so should be very simple process.

Import the database sql file to create the needed user table. This will also create a first user for you with the following login details:

email: admin@admin.com

password: password

** I highly recommend changing both the email and password directly after your first login **