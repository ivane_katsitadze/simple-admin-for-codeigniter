<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        
        if (!$this->session->userdata('user_id'))
        {
            if ($this->uri->segment(2) != 'login') {
                redirect('/admin/login');
            }
        }

        $this->load->model('admin_model');
    }

    public function index()
    {
        redirect('/admin/users');
    }

    // Users -------------------------------------------------------------------------------------

    /**
    *
    * Add, edit and remove users
    * @param string
    * @param int
    *
    **/
    public function users($page = NULL, $id = NULL)
    {
        $submit = $this->input->post();

        if ($submit) { // If we have a submit, we need to do something with the post data

            if ($this->input->post('id')) {
                
                if ($this->admin_model->update_user()) { // If we have an ID in the post array, then we are updating a user

                    $this->session->set_flashdata('user_update', 'User updated');

                } else {

                    $this->session->set_flashdata('user_update', 'Something went wrong, could not save changes');
                
                }

                redirect('admin/users/user/'.$this->input->post('id'));

            } else { // If we DONT have an ID in the post array, then we are creating a user

                if ($this->admin_model->new_user()) {

                    $this->session->set_flashdata('user_update', 'User created');

                } else {

                    $this->session->set_flashdata('user_update', 'Something went wrong, could not create the user');

                }

                redirect('admin/users/list');
            }

        } else { // If there is no submit data to take care of

            if ($id === 'new') { // If $id is 'new', view the create user view

                $data['pageview'] = 'admin/users/details';

            } elseif (!$id) { // If $id is 'null', view the create user list

                $data['users'] = $this->admin_model->get_users();
                $data['pageview'] = 'admin/users/list';

            } else { // else, we have a $id that is not 'new' or 'null', we are displaying a users info

                $data['user'] = $this->admin_model->get_user($id);
                $data['pageview'] = 'admin/users/details';

            }
        }

        $this->load->view('admin/wrapper', $data);
    }



    // Login & out -------------------------------------------------------------------------------------

    /**
    *
    * Login
    *
    **/
    public function login()
    {
        $post = $this->input->post();
        $data = array();

        if (!empty($post)) { // Login form posted, lets check credentials

            // Validate login credentials
            $login_status = $this->admin_model->login();

            if($login_status) { // If valid login, redirect to backend landing page

                redirect('/admin/users','refresh');

            } else { // Else, give back error message

                $data['login_error'] = 'Wrong email or password!';

            }

        }

        $this->load->view('admin/login', $data);
    }


    /**
    *
    * ALogout
    *
    **/
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('/admin/login','refresh');
    }
}