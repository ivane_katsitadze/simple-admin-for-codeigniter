<nav class="toolbar">
        
    <?php if ($this->session->userdata('user_id')) : ?>

        <?php

            $active = array();

            if ($this->uri->segment(2)) {
                $active[$this->uri->segment(2)] = 'class="active"';
            } else {
                $active['users'] = 'class="active"';
            }

        ?>

        <aside class="logged-in">

            <span class="static">Hello,</span>
            <span id="user"><?php echo $this->session->userdata('user_name'); ?></span>
            
        </aside>

        <ul class="toolmenu">
            
            <?php if ($this->session->userdata('user_access_level') === '10') : ?>
                <li><?php echo anchor('admin/users','Users', (!empty($active['users'])) ? $active['users'] : ''); ?></li>
            <?php endif; ?>

            <li><?php echo anchor('admin/logout','Logout'); ?></li>
        </ul>

    <?php endif; ?>

</nav>