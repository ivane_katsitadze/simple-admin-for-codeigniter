<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>CodeIgniter Simple Admin - Login</title>
    <link href="/assets/css/admin/normalize.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/assets/css/admin/basic.css" rel="stylesheet" type="text/css" media="screen" />
    
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
</head>
<body>

    <div class="login-wrapper">

        <?php if (!empty($login_error)) : ?>
            <div class="system-msg error"><?php echo $login_error; ?></div>
        <?php endif; ?>

        <div class="login">

            <form method="post">
                <input type="text" name="email" placeholder="Email" />
                <input type="password" name="password" placeholder="Password" />
                <input type="submit" name="login" value="Login" class="btn-type1" />
            </form>
            
        </div>

    </div>

</body>
</html>