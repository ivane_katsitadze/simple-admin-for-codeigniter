<section class="content_wrapper main-area">

    <div class="top-toolbar">

        <ul>
            <li>
                <?php echo anchor('admin/users/user/new', 'Add User'); ?>
            </li>
        </ul>

    </div>


    <div class="admin-view users">

        <?php if($this->session->flashdata('user_update')) : ?>
            <div class="system-msg"><?php echo $this->session->flashdata('user_update'); ?></div>
        <?php endif; ?>

        <table class="table-list">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>E-mail</th>
                    <th>Access Level</th>
                    <th></th>
                </tr>
            </thead>
            <?php $i=0; foreach($users as $user) : ?>
                <tr <?php echo ($i===0) ? 'class="even"' : 'class="odd"'; ?>>
                    <td><?php echo $user->id; ?></td>
                    <td><?php echo $user->name; ?></td>
                    <td><?php echo $user->email; ?></td>
                    <td><?php echo $user->access_level; ?></td>
                    <td><?php echo anchor('admin/users/user/'.$user->id,'Edit'); ?></td>
                </tr>
            <?php ($i===1) ? $i=0 : $i++; endforeach; ?>
        </table>
    </div>
</section>
