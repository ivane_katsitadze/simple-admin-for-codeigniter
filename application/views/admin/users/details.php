<section class="content_wrapper main-area">
    <div class="admin-view user">

        <?php if($this->session->flashdata('user_update')) : ?>
            <div class="system-msg"><?php echo $this->session->flashdata('user_update'); ?></div>
        <?php endif; ?>

        <h1>User: <?php echo (!empty($user->name)) ? $user->name : 'New user'; ?></h1>

        <form method="post">
            <?php if (!empty($user)) : ?>

                <input type="hidden" name="id" value="<?php echo $user->id; ?>" />

                <div class="label">Name</div>
                <input type="text" name="name" value="<?php echo $user->name; ?>" />
                <div class="label">E-mail</div>
                <input type="text" name="email" value="<?php echo $user->email; ?>" />
                <div class="label">Password</div>
                <input type="text" name="password" placeholder="Use only if you want to change the password!" />
                <div class="label">Access</div>
                <select name="access_level">
                    <option value="10" <?php echo ($user->access_level === '10') ? 'selected="selected"' : ''; ?>>10 - full</option>
                    <option value="9" <?php echo ($user->access_level === '9') ? 'selected="selected"' : ''; ?>>9</option>
                    <option value="8" <?php echo ($user->access_level === '8') ? 'selected="selected"' : ''; ?>>8</option>
                    <option value="7" <?php echo ($user->access_level === '7') ? 'selected="selected"' : ''; ?>>7</option>
                    <option value="6" <?php echo ($user->access_level === '6') ? 'selected="selected"' : ''; ?>>6</option>
                    <option value="5" <?php echo ($user->access_level === '5') ? 'selected="selected"' : ''; ?>>5</option>
                    <option value="4" <?php echo ($user->access_level === '4') ? 'selected="selected"' : ''; ?>>4</option>
                    <option value="3" <?php echo ($user->access_level === '3') ? 'selected="selected"' : ''; ?>>3</option>
                    <option value="2" <?php echo ($user->access_level === '2') ? 'selected="selected"' : ''; ?>>2</option>
                    <option value="1" <?php echo ($user->access_level === '1') ? 'selected="selected"' : ''; ?>>1 - basic</option>
                </select>

            <?php else : ?>

                <div class="label">Name</div>
                <input type="text" name="name" />
                <div class="label">E-mail</div>
                <input type="text" name="email" />
                <div class="label">Password</div>
                <input type="text" name="password" />
                <div class="label">Access Level</div>
                <select name="access_level">
                    <option value="10">10 - full</option>
                    <option value="9">9</option>
                    <option value="8">8</option>
                    <option value="7">7</option>
                    <option value="6">6</option>
                    <option value="5">5</option>
                    <option value="4">4</option>
                    <option value="3">3</option>
                    <option value="2">2</option>
                    <option value="1">1 - basic</option>
                </select>

            <?php endif; ?>

            <div class="actions-btns">
                <input type="submit" name="update_user" value="Submit" class="btn-type1" />
                <?php echo anchor('admin/users/','Back', 'class="btn-type2"'); ?>
            </div>

        </form>

    </div>
    
</section>