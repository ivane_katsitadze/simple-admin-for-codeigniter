<?php
// Load site header
$this->load->view('admin/header');

// Load view
$this->load->view($pageview);

// Load site footer
$this->load->view('admin/footer');
