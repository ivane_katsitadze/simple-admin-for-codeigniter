<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>CodeIgniter Simple Admin</title>
    <link href="/assets/css/admin/normalize.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/assets/css/admin/basic.css" rel="stylesheet" type="text/css" media="screen" />
    
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body class="adminarea">

<?php $this->load->view('/admin/menu'); ?>