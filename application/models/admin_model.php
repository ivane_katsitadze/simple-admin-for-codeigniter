<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model {

    // Users -------------------------------------------------------------------------------------

    /**
    *
    * Get all users
    *
    **/
    public function get_users()
    {
        
        $query = $this->db->get('users');
        return $query->result();
    }

    /**
    *
    * Get a single users info
    * @param int
    *
    **/
    public function get_user($user_id)
    {
        
        $this->db->where('id', $user_id);
        $query = $this->db->get('users');
        $result = $query->result();

        return $result[0];
    }

    /**
    *
    * Update a single users information
    *
    **/
    public function update_user()
    {
        $update = array(
            'name'          => $this->input->post('name'),
            'email'         => $this->input->post('email'),
            'access_level'  => $this->input->post('access_level')
        );

        if ($this->input->post('password')) {
            $update['password'] = sha1($this->input->post('password'));
        }

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('users', $update);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
    *
    * Create a new user
    *
    **/
    public function new_user()
    {
        $insert = array(
            'name'          => $this->input->post('name'),
            'email'         => $this->input->post('email'),
            'access_level'  => $this->input->post('access_level'),
            'password'      => sha1($this->input->post('password'))
        );

        $this->db->insert('users', $insert);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    // Login -------------------------------------------------------------------------------------

    /**
    *
    * Check login credentials
    *
    **/
    public function login()
    {
        $email = $this->input->post('email');
        $pw = sha1($this->input->post('password'));

        $this->db->where(array('email' => $email, 'password' => $pw));
        $q = $this->db->get('users');

        if($q->num_rows() > 0) {
            foreach($q->result() as $row) {
                $userdata = array(
                    'user_id'           => $row->id,
                    'user_email'        => $row->email,
                    'user_name'         => $row->name,
                    'user_access_level' => $row->access_level
                );
            }
            $this->session->set_userdata($userdata);
            return true;
        } else {
            return false;
        }
    }

}