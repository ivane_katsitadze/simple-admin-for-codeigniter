var hondaAdmin = {
    init : function() {
        //this.viewSize();
    },

    viewSize : function() {
        var lsize = '220';
        var rsize = $(window).width()-lsize;
       
        $('.main-area').css({
            'width' : rsize
        });

        $(window).resize(function() {
            var rsize = $(window).width()-lsize;

            $('.main-area').css({
                'width' : rsize
            });
        });
    }
};

$(document).ready(function(){
    hondaAdmin.init();
});